resource "aws_instance" "web" {
  ami           = data.aws_ami.amzlinuz.id
  instance_type = var.instance_type
  #count=1
  #key_name=TestKeyPair
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  tags = {
    Name = "Launch from terraform"
  }
}
#test
